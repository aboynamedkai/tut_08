#pragma once

#include "SpriteBatch.h"
#include "Sprite.h"
#include "SpriteFont.h"

class Game
{
public:
  enum class State { DRAW_CLOCKS, REPLAY };
  State state = State::DRAW_CLOCKS;

  Game(MyD3D& d3d);
  

  void Release();
  void Update(float dTime);
  void Render(float dTime);
private:
  MyD3D& mD3D;
  DirectX::SpriteBatch *pSB = nullptr;
  DirectX::SpriteFont *mpF = nullptr;
};