#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <vector>
#include <iomanip>

#include "WindowUtils.h"
#include "D3DUtil.h"
#include "CommonStates.h"
#include "SimpleMath.h"
#include "TexCache.h"
#include "game.h"
#include "time.h"
#include "gameData.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;


GameData gdata;



int getCurrentHours() {
  time_t now = time(0);
  struct tm& t = *localtime(&now);
  /*Alternatively to remove reporting these as errors or warnings
  open Project Properties > Configuration Properties > C/C++ > Preprocessor (as above)
  and paste in ;_CRT_SECURE_NO_WARNINGS into the Preprocessor Definitions box
  */
  //struct tm t;
  //localtime_s(&t, &now);
  return t.tm_hour;
}
int getCurrentMinutes() {
  time_t now = time(0);
  struct tm t;
  localtime_s(&t, &now);
  return t.tm_min;
}
int getCurrentSeconds() {
  time_t now = time(0);
  struct tm t;
  localtime_s(&t, &now);
  return t.tm_sec;
}
Time::Time()	//default constructor
  : hrs_(getCurrentHours()),
  mins_(getCurrentMinutes()),
  secs_(getCurrentSeconds())
{}
Time::~Time() {
  
}
int Time::getHours() const { 	//return a data member value, hrs_
  return hrs_;
}
int Time::getMinutes() const {	//return a data member value, mins_
  return mins_;
}
int Time::getSeconds() const {	//return a data member value, secs_
  return secs_;
}
void Time::setTime(int h, int m, int s) {	//set the time
  hrs_ = h;
  mins_ = m;
  secs_ = s;
}

//private support functions_______________________________________________
long long Time::toSeconds() const {	 //return time in seconds
  return ((hrs_ * 3600) + (mins_ * 60) + secs_);
}


Game::Game(MyD3D& d3d)
	: mD3D(d3d), pSB(nullptr)
{
	pSB = new SpriteBatch(&mD3D.GetDeviceCtx());
 
  mpF = new SpriteFont(&mD3D.GetDevice(), L"data/fonts/comicSansMS.spritefont");
  assert(mpF);
	TexCache& cache = mD3D.GetCache();
	cache.LoadTexture(&d3d.GetDevice(), "clock_face.dds");
	cache.LoadTexture(&d3d.GetDevice(), "clock_hands.dds");
	cache.LoadTexture(&d3d.GetDevice(), "digital_clock.dds");
}


//any memory or resources we made need releasing at the end
void Game::Release()
{
	delete pSB;
	pSB = nullptr;
  delete mpF;
  mpF = nullptr;
  
}

//called over and over, use it to update game logic
void Game::Update(float dTime)
{
  
 
}

//called over and over, use it to render things
void Game::Render(float dTime)
{
  Time t;
  float hourRotation;
  float minRotation;
  float hrs;
  string currentTime;
  stringstream ss;
  string timeFormat;
  double pi = 3.141592653589793238462643383279502884L;
  Vector2 pos;
  
  mD3D.BeginRender(Colours::Black);

  CommonStates dxstate(&mD3D.GetDevice());
  pSB->Begin(SpriteSortMode_Deferred, dxstate.NonPremultiplied(), &mD3D.GetWrapSampler());

  gdata.mssg = "TIME TO PLAY?";
  pos = Vector2(20.0f, 40.0f);
  mpF->DrawString(pSB, gdata.mssg.c_str(), pos);

  
  
  //clock face
  Sprite clockFace(mD3D);
  vector <Sprite> clockSprites(3, Sprite(clockFace));

  for (int i = 0; i < 3; i++)
  {
    clockSprites[i].SetTex(*mD3D.GetCache().Get("clock_face").pTex);
    clockSprites[i].SetScale(Vector2(0.4f, 0.4f));
  }

  clockSprites[0].SetPos(Vector2(50, 400));
  clockSprites[0].Draw(*pSB);

  clockSprites[1].SetPos(Vector2(400, 400));
  clockSprites[1].Draw(*pSB);

  clockSprites[2].SetPos(Vector2(750, 400));
  clockSprites[2].Draw(*pSB);
  //end clock face

 //clock min hands
  Sprite clockMinHands(mD3D);
  vector <Sprite> clockMinHandSprites(3, Sprite(clockMinHands));

  for (int i = 0; i < 3; i++)
  {
    clockMinHandSprites[i].SetTex(*mD3D.GetCache().Get("clock_hands").pTex);
    clockMinHandSprites[i].SetScale(Vector2(0.2f, 0.2f));
    clockMinHandSprites[i].SetTexRect({ 98 ,0, 145, 483 });
    clockMinHandSprites[i].SetOrigin(Vector2(25, 455));
  }

 
  minRotation = ((6 * (t.mins_ + 30)) * pi/180);
  clockMinHandSprites[1].SetPos(Vector2(500, 500));
  clockMinHandSprites[1].SetRotation(minRotation);
  clockMinHandSprites[1].Draw(*pSB);

  minRotation = ((60 * (t.mins_ - 60)) * pi/ 180);
  clockMinHandSprites[2].SetPos(Vector2(850, 500));
  clockMinHandSprites[2].SetRotation(minRotation);
  clockMinHandSprites[2].Draw(*pSB);

  minRotation = ((6 * t.mins_) * pi / 180);
  clockMinHandSprites[0].SetPos(Vector2(150, 500));
  clockMinHandSprites[0].SetRotation(minRotation);
  clockMinHandSprites[0].Draw(*pSB);
  //end clock min hands

  //clock hour hands
  Sprite clockHourHands(mD3D);
  vector <Sprite> clockHourHandSprites(3, Sprite(clockHourHands));

  for (int i = 0; i < 3; i++)
  {
    clockHourHandSprites[i].SetTex(*mD3D.GetCache().Get("clock_hands").pTex);
    clockHourHandSprites[i].SetScale(Vector2(0.2f, 0.2f));
    clockHourHandSprites[i].SetTexRect({ 7, 135, 90, 483 });
    clockHourHandSprites[i].SetOrigin(Vector2(47, 319));
    
  }
  
  hourRotation = ((30 * t.hrs_) * pi / 180);
  clockHourHandSprites[0].SetPos(Vector2(150, 500));
  clockHourHandSprites[0].SetRotation(hourRotation);
	clockHourHandSprites[0].Draw(*pSB);

  hourRotation = ((30 * (t.hrs_ + 40)) * pi/180);
  clockHourHandSprites[1].SetPos(Vector2(500, 500));
  clockHourHandSprites[1].SetRotation(hourRotation);
  clockHourHandSprites[1].Draw(*pSB);

  hourRotation = ((30 * (t.hrs_ - 30)) * pi/180);
  clockHourHandSprites[2].SetPos(Vector2(850, 500));
  clockHourHandSprites[2].SetRotation(hourRotation);
  clockHourHandSprites[2].Draw(*pSB);
  //end clock hour hands

 

  Sprite digitalClock(mD3D);
	digitalClock.SetTex(*mD3D.GetCache().Get("digital_clock").pTex);
	digitalClock.SetPos(Vector2(400, 150));
  digitalClock.SetScale(Vector2(0.4f, 0.4f));
	digitalClock.Draw(*pSB);

  if (t.hrs_ > 12)
  {
    hrs = t.hrs_ - 12;
  }
  else hrs = t.hrs_;

  if (t.hrs_ < 12)
  {
    timeFormat = "AM";
  }
  else
  {
    timeFormat = "PM";
  }

  pos = Vector2(530.f, 220.f);
  mpF->DrawString(pSB, timeFormat.c_str(), pos);

  ss << setw(2) << setfill('0') << hrs << ":" << setw(2) << setfill('0') << t.mins_ << ":" << setw(2) << setfill('0') << t.secs_;
  gdata.mssg2 = ss.str();
  pos = Vector2(440.0f, 190.0f);
  mpF->DrawString(pSB, gdata.mssg2.c_str(), pos);

  gdata.mssg3 = "Which clock shows the same time as the digital one? 1, 2 or 3";
  pos = Vector2(20.0f, 650.0f);
  mpF->DrawString(pSB, gdata.mssg3.c_str(), pos);

  gdata.clock1msg;
  pos = Vector2(50.0f, 400.0f);
  mpF->DrawString(pSB, gdata.clock1msg.c_str(), pos);

  gdata.clock2msg;
  pos = Vector2(400.0f, 400.0f);
  mpF->DrawString(pSB, gdata.clock2msg.c_str(), pos);

  gdata.clock3msg;
  pos = Vector2(750.0f, 400.0f);
  mpF->DrawString(pSB, gdata.clock3msg.c_str(), pos);

  gdata.mssg4;
  pos = Vector2(20.0f, 700.0f);
  mpF->DrawString(pSB, gdata.mssg4.c_str(), pos);


	pSB->End();
	mD3D.EndRender();
}


//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	d3d.OnResize_Default(screenWidth, screenHeight);
}

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
      break;
    case 49:
      gdata.mssg4 = "Correct display";
      break;
    case 50:
      gdata.mssg4 = "Wrong, try again";
      break;
    case 51:
      gdata.mssg4 = "Wrong, try again";
      break;
			return 0;
		}
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(1024), h(768);
	//int defaults[] = { 640,480, 800,600, 1024,768, 1280,1024 };
		//WinUtil::ChooseRes(w, h, defaults, 4);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "What time is it?", MainWndProc, true))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);
	d3d.GetCache().SetAssetPath("data/");
	Game game(d3d);

	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			game.Update(dTime);
			game.Render(dTime);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
	}

	game.Release();
	d3d.ReleaseD3D(true);	
	return 0;
}

