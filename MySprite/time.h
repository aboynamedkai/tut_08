#pragma once
#pragma once
#include <string>
#include <ctime>

class Time
{
public:
  Time();			//default constructor

  ~Time();			//destructor

  int getHours() const;	//return a data member value, hrs_
  int getMinutes() const;	//return a data member value, mins_
  int getSeconds() const;	//return a data member value, secs_
  void setTime(int, int, int);	//set the time to 3 given values
  int hrs_, mins_, secs_;
private:

  long long toSeconds() const;	//return the time in seconds
};